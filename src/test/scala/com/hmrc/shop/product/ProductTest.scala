package com.hmrc.shop.product

import org.specs2.mutable.Specification

/**
  * Created by TYCHULW on 27/07/2016.
  */
class ProductTest extends Specification {

  "Product" should {
    "be possible to create with proper name of oranges" in {
      ShopProduct(Orange.name) must beAnInstanceOf[Orange.type]
    }

    "be possible to create with proper name of Apples" in {
      ShopProduct(Apple.name) must beAnInstanceOf[Apple.type]
    }

    "be not possible to create unknown product" in {
      ShopProduct("dummyName") must throwA[IllegalArgumentException]
    }

  }

}
