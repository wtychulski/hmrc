package com.hmrc.shop.rule

import com.hmrc.shop.Checkout
import com.hmrc.shop.product.{Apple, Orange}
import org.specs2.mutable.Specification

/**
  * Created by TYCHULW on 27/07/2016.
  */
class RuleTest extends Specification {
  val buy2GetOneFree = new BuyTwoGetOneFree(Apple)
  val buy3forPriceOf2 = new BuyThreeForPriceOfTwo(Apple)
  val simpleCheckout = new Checkout(List())

  "Buy two get one free rule" should {

    "not reduce price if there is only one apples" in {
      val products = List(Apple)
      buy2GetOneFree.applyRule(products, Apple.price) must_== Apple.price
    }

    "reduce price by price of one if there two apples" in {
      val products = List(Apple, Apple)
      buy2GetOneFree.applyRule(products, Apple.price * 2) must_== Apple.price
    }

    "reduce price by price of one if there three apples" in {
      val products = List(Apple, Apple, Apple)
      buy2GetOneFree.applyRule(products, Apple.price * 3) must_== Apple.price * 2
    }

    "reduce price by price of 5 if there 10 apples" in {
      val products = List.fill(10)(Apple)
      buy2GetOneFree.applyRule(products, Apple.price * 10) must_== Apple.price * 5
    }

    "reduce price by price of one if there three apples and few oranges" in {
      val products = List(Apple, Orange, Apple, Apple, Orange)
      buy2GetOneFree.applyRule(products, Apple.price * 3 + Orange.price * 2) must_== Apple.price * 2 + Orange.price * 2
    }
  }



  "Buy three for price of tworule" should {

    "not reduce price if there is only one or two apples" in {
      val products = List(Apple)
      buy2GetOneFree.applyRule(products, Apple.price) must_== Apple.price
    }

    "not reduce price if there is only two apples" in {
      val products = List(Apple, Apple)
      buy2GetOneFree.applyRule(products, Apple.price * 2) must_== Apple.price * 2
    }

    "reduce price by price of one if there three apples" in {
      val products = List(Apple, Apple, Apple)
      buy2GetOneFree.applyRule(products, Apple.price * 3) must_== Apple.price * 2
    }

    "reduce price by price of one if there four or five apples" in {
      val products = List(Apple, Apple, Apple, Apple)
      buy3forPriceOf2.applyRule(products, Apple.price * 4) must_== Apple.price * 3

      val products2 = List(Apple, Apple, Apple, Apple, Apple)
      buy3forPriceOf2.applyRule(products2, Apple.price * 5) must_== Apple.price * 4
    }

    "reduce price by price of 5 if 15 apples" in {
      val products = List.fill(15)(Apple)
      buy3forPriceOf2.applyRule(products, Apple.price * 15) must_== Apple.price * 10

      val products2 = List(Apple, Apple, Apple, Apple, Apple)
      buy3forPriceOf2.applyRule(products2, Apple.price * 5) must_== Apple.price * 4
    }

    "reduce price by price of one if there three, four or five apples and few oranges" in {
      val products = List(Apple, Orange, Apple, Apple, Orange)
      buy3forPriceOf2.applyRule(products, Apple.price * 3 + Orange.price * 2) must_==
        Apple.price * 2 + Orange.price * 2

      val products2 = List(Apple, Orange, Apple, Apple, Orange)
      buy3forPriceOf2.applyRule(products2, Apple.price * 4 + Orange.price * 2) must_==
        Apple.price * 3 + Orange.price * 2

      val products3 = List(Apple, Orange, Apple, Apple, Orange)
      buy3forPriceOf2.applyRule(products3, Apple.price * 5 + Orange.price * 2) must_==
        Apple.price * 4 + Orange.price * 2
    }
  }

}
