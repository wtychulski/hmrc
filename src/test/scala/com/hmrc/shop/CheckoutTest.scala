package com.hmrc.shop

import com.hmrc.shop.product.{Apple, Orange}
import org.specs2.mutable.Specification

/**
  * Created by Wiktor Tychulski on 27.07.2016.
  */
class CheckoutTest extends Specification {

  val simpleCheckout = new Checkout()

  "Checkout" should {

    "calculate final price of empty product list" in {
      simpleCheckout.getTotal(List()) must_== 0
    }

    "calculate final price for 1 apple" in {
      simpleCheckout.getTotal(List(Apple)) must_== Apple.price
    }

    "calculate final price for 1 orange" in {
      simpleCheckout.getTotal(List(Orange)) must_== Orange.price
    }

    "calculate total price for mixed type of product" in {
      simpleCheckout.getTotal(List(Orange, Apple, Orange, Orange)) must_== 3 * Orange.price + Apple.price
    }
  }

}
