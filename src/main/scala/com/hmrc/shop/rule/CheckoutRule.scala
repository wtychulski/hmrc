package com.hmrc.shop.rule

import com.hmrc.shop.product.ShopProduct

/**
  * Created by TYCHULW on 27/07/2016.
  */
sealed trait CheckoutRule {
  val productInPromotion: ShopProduct
  def applyRule(products: List[ShopProduct], totalPrice: Int): Int
}

class BuyTwoGetOneFree(val productInPromotion: ShopProduct) extends CheckoutRule {

  override def applyRule(products: List[ShopProduct], totalPrice: Int): Int = {
    val count = products.count(prod => prod == productInPromotion)
    if (count > 1) {
      val productPrice = products.find(_ == productInPromotion).get.price
      totalPrice - (count / 2) * productPrice
    } else {
      totalPrice
    }
  }
}

class BuyThreeForPriceOfTwo(val productInPromotion: ShopProduct) extends CheckoutRule {

  override def applyRule(products: List[ShopProduct], totalPrice: Int): Int = {
    val count = products.count(prod => prod == productInPromotion)
    if (count > 2) {
      val productPrice = products.find(_ == productInPromotion).get.price
      totalPrice - (count / 3) * productPrice
    } else {
      totalPrice
    }
  }
}
