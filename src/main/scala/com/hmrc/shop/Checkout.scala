package com.hmrc.shop

import com.hmrc.shop.product.ShopProduct
import com.hmrc.shop.rule.CheckoutRule

/**
  * Created by Wiktor Tychulski on 27.07.2016.
  */
class Checkout(checkoutAdditionalRules: List[CheckoutRule]) {


  def this() = this(List())

  def getTotal(products: List[ShopProduct]): Int = {
    val total = products.foldLeft(0)((acc, product) => acc + product.price)
    checkoutAdditionalRules.foldLeft(total)((newTotal, rule) => rule.applyRule(products, newTotal))
  }

}
