package com.hmrc.shop.product

/**
  * Created by TYCHULW on 27/07/2016.
  */
sealed trait ShopProduct {
  val name: String
  val price: Int
}

object ShopProduct {
  def apply(productName: String): ShopProduct = productName match {
    case Orange.name => Orange
    case Apple.name => Apple
    case _: String => throw new IllegalArgumentException("Unknown product type: " + productName)
  }
}

case object Orange extends ShopProduct {
  override val name: String = "Orange"
  override val price: Int = 25
}

case object Apple extends ShopProduct {
  override val name: String = "Apple"
  override val price: Int = 60
}
