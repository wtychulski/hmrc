package com.hmrc.shop

import com.hmrc.shop.product.{Apple, Orange, ShopProduct}
import com.hmrc.shop.rule.{BuyThreeForPriceOfTwo, BuyTwoGetOneFree}

/**
  * Created by Wiktor Tychulski on 27.07.2016.
  */
object HmrcApplication {


  def main(args: Array[String]) {
    val products = for {arg <- args} yield ShopProduct(arg)
    products.foreach(println(_))
    val total = new Checkout().getTotal(products.toList)
    println(s"Total price is: ${total * 1.0d/100} GBP")


    println("Buy two get one free")
    val total2for1 = new Checkout(List(new BuyTwoGetOneFree(Apple))).getTotal(products.toList)
    println(s"Total price is: ${total2for1 * 1.0d/100} GBP")


    println("Buy 3 for price of 2")
    val totalReduces3for2 = new Checkout(List(new BuyThreeForPriceOfTwo(Orange))).getTotal(products.toList)
    println(s"Total price is: ${totalReduces3for2 * 1.0d/100} GBP")
  }
}
